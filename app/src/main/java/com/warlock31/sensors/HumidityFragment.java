package com.warlock31.sensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Warlock on 2/4/2016.
 */
public class HumidityFragment extends Fragment{
    TextView humidityReading;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vi = inflater.inflate(R.layout.humidity, container, false);


        humidityReading = (TextView) vi.findViewById(R.id.humidity_reading);
        SensorManager mySensorManager = (SensorManager) getActivity().getSystemService(getActivity().SENSOR_SERVICE);

        Sensor TemperatureSensor = mySensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);


        if (TemperatureSensor != null) {
            // textAMBIENT_TEMPERATURE_available.setText("Sensor.TYPE_AMBIENT_TEMPERATURE Available");
            mySensorManager.registerListener(
                    HumiditySensorListener,
                    TemperatureSensor,
                    SensorManager.SENSOR_DELAY_NORMAL);
        } else {
            humidityReading.setText("Sensor.TYPE_AMBIENT_TEMPERATURE NOT Available");
        }

        return vi;

    }

    private final SensorEventListener HumiditySensorListener
            = new SensorEventListener() {

        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {

            String humidityValue = String.format("%.0f", sensorEvent.values[0]);
            humidityReading.setText(humidityValue + "%");
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
    };

}
