package com.warlock31.sensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Warlock on 2/2/2016.
 */
public class CompassFragment extends Fragment implements SensorEventListener {

    private ImageView imageView;

    private float currentDegree =0f;

    private SensorManager sensorManager;

    TextView textView;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vi = inflater.inflate(R.layout.compass, container, false);

        imageView = (ImageView) vi.findViewById(R.id.imageViewCompass);
        textView = (TextView) vi.findViewById(R.id.tvHeading);

      sensorManager = (SensorManager) getActivity().getSystemService(getActivity().SENSOR_SERVICE);

        return vi;

    }

    @Override
    public void onResume() {
        super.onResume();

        // for the system's orientation sensor registered listeners
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    public void onPause() {
        super.onPause();

        // to stop the listener and save battery
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        float degree = Math.round(sensorEvent.values[0]);

        textView.setText("Heading:" + Float.toString(degree)+ " degrees");

        RotateAnimation rotateAnimation = new RotateAnimation(currentDegree,-degree, Animation.RELATIVE_TO_SELF,0.5f,Animation.RELATIVE_TO_SELF,0.5f);

        rotateAnimation.setDuration(210);
        rotateAnimation.setFillAfter(true);

        imageView.startAnimation(rotateAnimation);
        currentDegree = -degree;

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
