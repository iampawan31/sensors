package com.warlock31.sensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Warlock on 2/10/2016.
 */
public class ProximityFragment extends Fragment {

    TextView proximityReading;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vi = inflater.inflate(R.layout.proximity, container, false);


        proximityReading = (TextView) vi.findViewById(R.id.proximity_reading);
        SensorManager mySensorManager = (SensorManager) getActivity().getSystemService(getActivity().SENSOR_SERVICE);

        Sensor ProximitySensor = mySensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);


        if (ProximitySensor != null) {
            // textAMBIENT_TEMPERATURE_available.setText("Sensor.TYPE_AMBIENT_TEMPERATURE Available");
            mySensorManager.registerListener(
                    ProximitySensorListener,
                    ProximitySensor,
                    SensorManager.SENSOR_DELAY_NORMAL);
        } else {
            proximityReading.setText("Sensor.TYPE_AMBIENT_TEMPERATURE NOT Available");
        }

        return vi;

    }

    private final SensorEventListener ProximitySensorListener
            = new SensorEventListener() {

        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {

           if (sensorEvent.values[0] == 0){
               proximityReading.setText("Too Close");
           }
            else{
               proximityReading.setText("Too Far");
           }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
    };
}
