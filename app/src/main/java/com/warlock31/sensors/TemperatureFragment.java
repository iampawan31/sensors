package com.warlock31.sensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Warlock on 2/2/2016.
 */
public class TemperatureFragment extends Fragment {
    private int isCelsius = 1;
    TextView textTEMPERATURE_available, textTEMPERATURE_reading;
    TextView textAMBIENT_TEMPERATURE_available, textAMBIENT_TEMPERATURE_reading;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vi = inflater.inflate(R.layout.temperature, container, false);

        final Button change_type = (Button) vi.findViewById(R.id.change_type);

        change_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isCelsius == 1) {
                    isCelsius = 0;
                    change_type.setText("Show Celsius");
                } else {
                    isCelsius = 1;
                    change_type.setText("Show Fahrenheit");
                }
            }
        });

        textAMBIENT_TEMPERATURE_reading = (TextView) vi.findViewById(R.id.AMBIENT_TEMPERATURE_reading);
        SensorManager mySensorManager = (SensorManager) getActivity().getSystemService(getActivity().SENSOR_SERVICE);

        Sensor TemperatureSensor = mySensorManager.getDefaultSensor(Sensor.TYPE_TEMPERATURE);
        if(TemperatureSensor != null){
            // textTEMPERATURE_available.setText("Sensor.TYPE_TEMPERATURE Available");
            mySensorManager.registerListener(
                    TemperatureSensorListener,
                    TemperatureSensor,
                    SensorManager.SENSOR_DELAY_NORMAL);

        }else{
             //textTEMPERATURE_available.setText("Sensor.TYPE_TEMPERATURE NOT Available");
        }

        Sensor AmbientTemperatureSensor
                = mySensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        if(AmbientTemperatureSensor != null){
            // textAMBIENT_TEMPERATURE_available.setText("Sensor.TYPE_AMBIENT_TEMPERATURE Available");
            mySensorManager.registerListener(
                    AmbientTemperatureSensorListener,
                    AmbientTemperatureSensor,
                    SensorManager.SENSOR_DELAY_NORMAL);
        }else{
            //textAMBIENT_TEMPERATURE_available.setText("Sensor.TYPE_AMBIENT_TEMPERATURE NOT Available");
        }

        return vi;

    }

    private final SensorEventListener TemperatureSensorListener
            = new SensorEventListener(){

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            if(event.sensor.getType() == Sensor.TYPE_TEMPERATURE){
                //textTEMPERATURE_reading.setText("TEMPERATURE: " + event.values[0]);
            }
        }

    };

    private final SensorEventListener AmbientTemperatureSensorListener
            = new SensorEventListener(){

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            if(event.sensor.getType() == Sensor.TYPE_AMBIENT_TEMPERATURE){
                if (isCelsius == 1){
                    String temp = String.format("%.2f",event.values[0]);
                    textAMBIENT_TEMPERATURE_reading.setText(temp + "°C");
                }else {
                    // String new_temp = String.format(celsius * 9) / 5) + 32;
                    String temp = String.format("%.2f",((event.values[0] * 9)/ 5)+32);
                    textAMBIENT_TEMPERATURE_reading.setText(temp + "°F");
                }
            }
        }

    };
}
